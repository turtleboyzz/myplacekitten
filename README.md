# React Native Boiletplate Instruction Manual

## IDE

VSCode is the IDE of choice for using with this boilerplate.

Install the following extentions for better experience:

- ES7 React/Redux/GraphQL/React-Native snippets
- ESLint
- Prettier
- GitLens
- DotENV
- Color Highlight

## Folder Structure

This boilerplate partially implement duck pattern when it comes to folder structure and is heavily influenced by Ignite boilerplate.

```
├── src
│   ├── Assets                          --> folder containing all the assets in relation to font icon and image.
│       ├── Fonts                       --> contain all fonts that are used.
│       ├── Icons                       --> contain all svg files that are used as icons.
│       ├── Images                      --> contain all png/jpg files that are used as images.
│       ├── index.js                    --> the main import and export.
│
│   ├── Components                      --> folder containing all custom-built components that are used project-wide.
│       ├── index.js                    --> the main import and export.
│
│   ├── Configs                         --> folder containing all config. eg. amount format, date format
│       ├── index.js
│
│   ├── Constants                       --> folder containing all project-wide constants.
│       ├── index.js
│
│   ├── Libs                            --> Libs are function helpers that have no dependencies.
│       ├── index.js
│
│   ├── Locales                         --> folder containing all different languages json
│       ├── index.js
│
│   ├── Navigation                      --> folder containing all navigation routes, stack and main navigation.
│       ├── Stacks                      --> folder containing stacks navigation
│           ├── index.js
|       ├── NavigationHelper.js         --> helper functions for using to navigate around the app.
│       ├── RootNavigation.js           --> main navigation where all stacks are combined and export to app.
│
│   ├── Redux                           --> folder containing all redux and redux saga within the app.
|       ├── ExampleRedux                --> folder of a redux
|           ├── Action.js               --> file containing all Actions related to ExampleRedux
|           ├── Reducer.js              --> file containing the selector, initialState, and reducer of ExampleRedux
|           ├── Saga.js                 --> file containing of Saga handler related to ExampleRedux
│       ├── CombineReducers.js          --> this is where all the reducers are imported and combined for CreateStore
│       ├── CreateStore.js              --> this is where all redux middlewares and sagas come together forming Redux
│       ├── RootSaga.js                 --> this is where all the saga are forked
│
│   ├── Screens                         --> folder containing all of our screen for the app
│       ├── Example
│           ├── Components              --> an optional folder for storing components that are used only within this screen.
│           ├── ExampleScreen.js        --> an exmaple of a screen file.
│       ├── index.js                    --> the main import and export to Navigation and Stacks.
│       ├── App.js                      --> we bind our app with redux here.
│
│   ├── Services                        --> anything or instances that require using a third-party services goes here
│       ├── index.js
│
│   ├── Themes                          --> anything that defines the look and feel of our app goes here
│       ├── Colors.js                   --> the colors and palette of our app
│       ├── Fonts.js                    --> everything related to fonts. eg. fontsize, fontstyle, fontfamily
│       ├── Metrics.js                  --> measurements to use within the app. eg. padding, margin, borderWidth, iconSize, imageSize.
│       ├── index.js
│
│   ├── Transforms                      --> folder containing functions to transform response data from service to our app state
│       ├── Example.js                  --> an example of a transform file which will include function fromResponse() and toPayload()
│       ├── index.js
│
│   ├── Utils                           --> folder containing utitlities functions that has dependencies
│       ├── index.js
│
├── .env                                --> our environment file
│
```

## Plugins

### State Management and Side-Effects handler

This boilerplate uses Redux for state management and Redux-Saga to handle side-effect.

- redux
- react-redux
  > redux and react-redux is the backbone of the state management across the entirety of the project.
- redux-logger
  > redux-logger is mainly for debugging the action affecting the state of the app.
- redux-persist
  > redux-persist is a way to save current state of the app.
- reselect
  > reselect is your main way of selecting a value from the app state onto components.
- seamless-immutable
  > seamless-immutable is a lifesaver for those who don't want to deal with immutable.
- redux-saga
  > redux-saga will be our main way of handling side-effects

### Localization

`react-i18next` is the plugin of choice for localization due to its simplicity of uses and hook supports.

### Api

`apisauce` is a simplified version of `axios` and is incredibly useful for fetching or posting an api.

### Navigation

`React Navigation` is second to none and is used in this project

## How to code

This project template is strictly adhere to a few rules of which can be found in the `.elintrc.js` file. These rules are meant to ensure everyone follow a certain bare minimum standard of coding.
When rules are not followed,

- There will be red-squiggly-line under your code
- You will be prevented from committing your code
