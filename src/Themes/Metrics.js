import { Dimensions, Platform, StatusBar } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { moderateScale } from 'react-native-size-matters';

const { width, height } = Dimensions.get('window');

const MobileMetrics = {
  // Text
  Font: {
    Header: moderateScale(20),
    Title: moderateScale(16),
    Subtitle: moderateScale(14),
    Body: moderateScale(14),
    Paragraph: moderateScale(12),
    Button: moderateScale(14),
    Caption: moderateScale(12),
    Note: moderateScale(10),
  },

  // Padding
  PageHorizontalPadding: moderateScale(16),
  PageVerticalPadding: moderateScale(8),

  // Border Radius
  DefaultRadius: moderateScale(6),

  // Component Size
  DefaultComponentSize: moderateScale(40),

  // Gap
  SmallGap: moderateScale(4), // Smallest Gap should be
  DefaultGap: moderateScale(8), // Default size for an average gap
  BigGap: moderateScale(12), // Big Gap usually for differentiate between sections
  LargestGap: moderateScale(16), // No use, probably

  Divider: moderateScale(2),
  // Screen Size
  ScreenWidth: width,
  ScreenHeight: height - Platform.select({ ios: getStatusBarHeight(true), android: StatusBar.currentHeight }),

  Icon: {
    DefaultIconSize: moderateScale(20),
    BigIconSize: moderateScale(30),
    DrawerSize: moderateScale(16),
  },
  Image: {
    Avatar: moderateScale(72),
  },
};

export default MobileMetrics;
