import { Appearance } from 'react-native';

const LightTheme = {
  Primary: '#673ab7',
  PrimaryLight: '#9a67ea',
  PrimaryDark: '#320b86',
  Error: '#f44336',
  Success: '#4caf50',

  //    Text
  TextPrimary: 'rgba(0, 0, 0, 0.87)',
  TextSecondary: 'rgba(0, 0, 0, 0.54)',
  TextDisabled: 'rgba(0, 0, 0, 0.38)',

  //   Button
  ButtonActive: 'rgba(0, 0, 0, 0.54)',
  ButtonActionDisabled: 'rgba(0, 0, 0, 0.26)',
  ButtonDisabled: 'rgba(0, 0, 0, 0.12)',

  //   Background
  BackgroundDefault: '#efefef',
  BackgroundPaper: '#fff',
  ModalBackground: `rgba(0, 0, 0, 0.6)`,

  //   Divider
  Divider: 'rgba(0, 0, 0, 0.12)',
};

const DarkTheme = {
  Primary: '#b39ddb',
  PrimaryLight: '#e6ceff',
  PrimaryDark: '#836fa9',
  Error: '#ef9a9a',
  Success: '#a5d6a7',

  //    Text
  TextPrimary: '#fff',
  TextSecondary: 'rgba(255, 255, 255, 0.7)',
  TextDisabled: 'rgba(255, 255, 255, 0.5)',

  //   Button
  ButtonActive: '#fff',
  ButtonActionDisabled: 'rgba(255, 255, 255, 0.3)',
  ButtonDisabled: 'rgba(255, 255, 255, 0.12)',

  //   Background
  BackgroundDefault: '#303030',
  BackgroundPaper: '#424242',
  ModalBackground: `rgba(0, 0, 0, 0.6)`,

  //   Divider
  Divider: 'rgba(255, 255, 255, 0.12)',
};

const ColorScheme = Appearance.getColorScheme() === 'dark' ? DarkTheme : LightTheme;

export default ColorScheme;
