import { Colors } from '../../Themes';

export default {
  headerStyle: {
    backgroundColor: Colors.BackgroundPaper,
    borderColor: Colors.Divider,
  },
  headerTitleStyle: {
    color: Colors.TextPrimary,
  },
  headerTintColor: Colors.Primary,
};
