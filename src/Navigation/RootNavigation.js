import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { navigationRef, isReadyRef } from './NavigationHelper';
import { ScreenConstant } from '../Constants';
import { KittenDetailScreen, KittenListScreen, NoConnectionScreen } from '../Screens';
import StackHeaderStyle from './Styles/StackHeaderStyle';

const RootStack = createStackNavigator();

export default function RootNavigation() {
  const { t } = useTranslation();

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={async () => {
        console.log('Navigation is ready');
        isReadyRef.current = true;
      }}>
      <RootStack.Navigator
        screenOptions={StackHeaderStyle}
        mode="card"
        initialRouteName={ScreenConstant.KITTEN_LIST_SCREEN}>
        <RootStack.Screen
          options={{ title: t(`screen.kittenDetailScreen.headerTitle`) }}
          name={ScreenConstant.KITTEN_DETAIL_SCREEN}
          component={KittenDetailScreen}
        />
        <RootStack.Screen
          options={{ title: t(`screen.kittenListScreen.headerTitle`) }}
          name={ScreenConstant.KITTEN_LIST_SCREEN}
          component={KittenListScreen}
        />
        <RootStack.Screen
          options={{ headerShown: false }}
          name={ScreenConstant.NO_CONNECTION_SCREEN}
          component={NoConnectionScreen}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
