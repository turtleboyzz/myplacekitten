import React from 'react';
import { StackActions } from '@react-navigation/native';

export const isMountedRef = React.createRef();

export const navigationRef = React.createRef();

export const isReadyRef = React.createRef();

// Only Use this if you don't have access to navigation. i.e Saga
class NavigationHelper {
  static instance() {
    return navigationRef.current;
  }

  static navigate(name, params = {}) {
    if (!navigationRef.current) return;
    navigationRef.current.navigate(name, params);
  }

  static push(name, params = {}) {
    if (!navigationRef.current) return;
    navigationRef.current.dispatch(StackActions.push(name, params));
  }

  static replace(name, params = {}) {
    if (!navigationRef.current) return;
    navigationRef.current.dispatch(StackActions.replace(name, params));
  }

  static back() {
    if (!navigationRef.current) return;
    navigationRef.current.goBack();
  }

  static reset(name, params = {}) {
    if (!navigationRef.current) return;
    navigationRef.current.dispatch(StackActions.replace(name, params));
  }
}

export default NavigationHelper;
