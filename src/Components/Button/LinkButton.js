import React from 'react';
import styled from 'styled-components/native';
import { Colors, Metrics } from '../../Themes';
import NoteText from '../Text/NoteText';

// Styled components go here
const Styled = {
  Wrapper: styled.TouchableOpacity`
    min-height: ${Metrics.DefaultComponentSize}px;
    padding-horizontal: ${Metrics.DefaultGap}px;
    justify-content: center;
    align-items: center;
  `,
};

function LinkButton({ onPress = () => {}, children, disabled = false, style = {}, color = Colors.Primary }) {
  return (
    <Styled.Wrapper activeOpacity={0.8} onPress={onPress} disabled={disabled} style={style}>
      <NoteText color={color} fontWeight="bold" style={{ textTransform: 'uppercase' }}>
        {children}
      </NoteText>
    </Styled.Wrapper>
  );
}

export default LinkButton;
