import React from 'react';
import styled from 'styled-components/native';
import Icons from 'react-native-vector-icons/Ionicons';
import { Colors, Metrics } from '../../Themes';

// Styled components go here

const ButtonWrapper = styled.TouchableOpacity.attrs(props => ({
  backgroundColor: props.backgroundColor,
  borderColor: props.borderColor,
  buttonSize: props.buttonSize || Metrics.DefaultComponentSize,
}))`
  width: ${props => props.buttonSize}px;
  height: ${props => props.buttonSize}px;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.backgroundColor};
  border-width: 2px;
  border-color: ${props => props.borderColor};
  border-radius: ${Metrics.DefaultRadius}px;
`;

function IconButton({
  iconName = 'grid-outline',
  buttonSize = Metrics.DefaultComponentSize,
  iconSize = Metrics.Icon.DefaultIconSize,
  iconColor = Colors.TextPrimary,
  buttonColor = Colors.Primary,
  borderColor = Colors.Primary,
  onPress = () => {},
  disabled = false,
  style = {},
}) {
  return (
    <ButtonWrapper
      activeOpacity={0.8}
      backgroundColor={buttonColor}
      borderColor={borderColor}
      onPress={onPress}
      buttonSize={buttonSize}
      disabled={disabled}
      style={style}>
      {/* {children} */}
      <Icons name={iconName} size={iconSize} color={iconColor} />
    </ButtonWrapper>
  );
}

export default IconButton;
