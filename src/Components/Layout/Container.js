import styled from 'styled-components/native';
import { Colors, Metrics } from '../../Themes';

const Container = styled.View.attrs(props => ({
  flexDirection: props.flexDirection || 'column',
  justifyContent: props.justifyContent || 'center',
  alignItems: props.alignItems || 'center',
  paddingVertical: props.paddingVertical >= 0 ? props.paddingVertical : Metrics.PageVerticalPadding,
  paddingHorizontal: props.paddingHorizontal >= 0 ? props.paddingHorizontal : Metrics.PageHorizontalPadding,
  backgroundColor: props.backgroundColor || Colors.BackgroundDefault,
  height: props.height || Metrics.ScreenHeight,
  width: props.width || Metrics.ScreenWidth,
}))`
  flex: 1;
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  background-color: ${props => props.backgroundColor};
  flex-direction: ${props => props.flexDirection};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  padding-vertical: ${props => props.paddingVertical}px;
  padding-horizontal: ${props => props.paddingHorizontal}px;
`;

export default Container;
