import styled from 'styled-components/native';

const FixedWrapper = styled.View.attrs(props => ({
  width: props.width ? `${props.width}px` : '100%',
  backgroundColor: props.backgroundColor || 'transparent',
  paddingHorizontal: props.paddingHorizontal || 0,
  paddingVertical: props.paddingVertical || 0,
  flexDirection: props.flexDirection || 'row',
  justifyContent: props.justifyContent || 'center',
  alignItems: props.alignItems || 'flex-start',
  alignSelf: props.alignSelf || 'center',
  borderRadius: props.borderRadius || 0,
}))`
  width: ${props => props.width};
  background-color: ${props => props.backgroundColor};
  flex-direction: ${props => props.flexDirection};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  align-self: ${props => props.alignSelf};
  padding-vertical: ${props => props.paddingVertical}px;
  padding-horizontal: ${props => props.paddingHorizontal}px;
  border-radius: ${props => props.borderRadius}px;
`;

export default FixedWrapper;
