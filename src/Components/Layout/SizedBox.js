import { View } from 'react-native';
import styled from 'styled-components/native';

const SizedBox = styled(View).attrs(props => ({
  height: props.height ? `${props.height}px` : '100%',
  width: props.width ? `${props.width}px` : '100%',
  backgroundColor: props.backgroundColor || 'transparent',
}))`
  height: ${props => props.height};
  width: ${props => props.width};
  background-color: ${props => props.backgroundColor};
`;

export default SizedBox;
