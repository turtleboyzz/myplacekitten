import styled from 'styled-components/native';

const FlexibleWrapper = styled.View.attrs(props => ({
  backgroundColor: props.backgroundColor || 'transparent',
  paddingHorizontal: props.paddingHorizontal || 0,
  paddingVertical: props.paddingVertical || 0,
  flexDirection: props.flexDirection || 'row',
  justifyContent: props.justifyContent || 'center',
  alignItems: props.alignItems || 'center',
  borderRadius: props.borderRadius || 0,
}))`
  background-color: ${props => props.backgroundColor};
  flex-direction: ${props => props.flexDirection};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  padding-vertical: ${props => props.paddingVertical}px;
  padding-horizontal: ${props => props.paddingHorizontal}px;
  border-radius: ${props => props.borderRadius}px;
`;

export default FlexibleWrapper;
