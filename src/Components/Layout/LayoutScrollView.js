import React from 'react';
import { Appearance, KeyboardAvoidingView, SafeAreaView, View, StatusBar } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';
import { Colors } from '../../Themes';

const Styled = {
  KeyboardView: styled(KeyboardAvoidingView).attrs(props => ({
    backgroundColor: props.backgroundColor,
  }))`
    flex: 1;
    background-color: ${props => props.backgroundColor};
  `,
  SafeArea: styled(SafeAreaView).attrs(props => ({
    backgroundColor: props.backgroundColor,
    flexDirection: props.flexDirection || 'column',
    justifyContent: props.justifyContent || 'flex-start',
    alignItems: props.alignItems || 'center',
  }))`
    flex: 1;
    height: 100%;
    background-color: ${props => props.backgroundColor};
    flex-direction: ${props => props.flexDirection};
    justify-content: ${props => props.justifyContent};
    align-items: ${props => props.alignItems};
  `,
  Container: styled(View).attrs(props => ({
    backgroundColor: props.backgroundColor,
    flexDirection: props.direction || 'column',
    justifyContent: props.justifyContent || 'flex-start',
    alignItems: props.alignItems || 'center',
  }))`
    height: 100%;
    background-color: ${props => props.backgroundColor};
    flex-direction: ${props => props.flexDirection};
    justify-content: ${props => props.justifyContent};
    align-items: ${props => props.alignItems};
  `,
};

export default function LayoutScrollView({
  backgroundColor = Colors.BackgroundDefault,
  barStyle = Appearance.getColorScheme() === 'dark' ? 'light-content' : 'dark-content',
  children,
  flexDirection = 'column',
  justifyContent = 'flex-start',
  alignItems = 'center',
  useScroll = false,
}) {
  return (
    <>
      <StatusBar barStyle={barStyle} backgroundColor={Colors.BackgroundPaper} />
      <Styled.SafeArea
        backgroundColor={backgroundColor}
        flexDirection={flexDirection}
        justifyContent={justifyContent}
        alignItems={alignItems}>
        {useScroll && (
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps="handled"
            style={{ height: '100%' }}
            // enableAutomaticScroll
            contentContainerStyle={{
              flexDirection,
              justifyContent,
              alignItems,
            }}
            showsVerticalScrollIndicator={false}>
            {children}
          </KeyboardAwareScrollView>
        )}
        {!useScroll && children}
      </Styled.SafeArea>
    </>
  );
}
