import styled from 'styled-components/native';

const SplitWrapper = styled.View.attrs(props => ({
  flex: props.flex || 1,
  backgroundColor: props.backgroundColor || 'transparent',
  paddingHorizontal: props.paddingHorizontal || 0,
  paddingVertical: props.paddingVertical || 0,
  flexDirection: props.flexDirection || 'row',
  justifyContent: props.justifyContent || 'center',
  alignItems: props.alignItems || 'flex-start',
  borderRadius: props.borderRadius || 0,
  alignSelf: props.alignSelf || 'stretch',
}))`
  flex: ${props => props.flex};
  background-color: ${props => props.backgroundColor};
  flex-direction: ${props => props.flexDirection};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  padding-vertical: ${props => props.paddingVertical}px;
  padding-horizontal: ${props => props.paddingHorizontal}px;
  border-radius: ${props => props.borderRadius}px;
  align-self: ${props => props.alignSelf};
`;

export default SplitWrapper;
