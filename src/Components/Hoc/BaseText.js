import Styled from 'styled-components/native';
import { Colors } from '../../Themes';

const BaseText = Styled.Text.attrs(props => ({
  color: props.color || Colors.TextPrimary,
  textAlign: props.textAlign || 'left',
  alignSelf: props.alignSelf || 'flex-start',
  fontWeight: props.fontWeight || 'normal',
}))`
    color : ${props => props.color};
    text-align: ${props => props.textAlign};
    align-self : ${props => props.alignSelf};
    font-weight : ${props => props.fontWeight};
`;

export default BaseText;
