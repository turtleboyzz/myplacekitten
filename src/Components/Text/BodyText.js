import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const BodyText = Styled(BaseText)`
    font-size : ${Metrics.Font.Body}px;
`;

export default BodyText;
