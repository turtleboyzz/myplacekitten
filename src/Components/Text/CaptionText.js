import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const CaptionText = Styled(BaseText).attrs(props => ({
  textTransform: props.textTransform || 'none',
}))`
    font-size : ${Metrics.Font.Caption}px;
    font-weight: bold;
    text-transform : ${props => props.textTransform};
`;

export default CaptionText;
