import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const TitleText = Styled(BaseText)`
    font-size : ${Metrics.Font.Title}px;
`;

export default TitleText;
