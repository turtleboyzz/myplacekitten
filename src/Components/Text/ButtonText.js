import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const ButtonText = Styled(BaseText)`
    font-size : ${Metrics.Font.Button}px;
    text-transform : uppercase;
`;

export default ButtonText;
