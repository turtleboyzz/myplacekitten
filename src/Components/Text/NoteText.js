import Styled from 'styled-components';
import { Colors, Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const NoteText = Styled(BaseText).attrs(props => ({
  color: props.color || Colors.TextSecondary,
}))`
    font-size : ${Metrics.Font.Note}px;
    color : ${props => props.color};
`;

export default NoteText;
