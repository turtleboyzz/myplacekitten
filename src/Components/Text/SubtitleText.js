import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const SubtitleText = Styled(BaseText)`
    font-size : ${Metrics.Font.Subtitle}px;
`;

export default SubtitleText;
