import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const ParagraphText = Styled(BaseText)`
    font-size : ${Metrics.Font.Paragraph}px;
`;

export default ParagraphText;
