import Styled from 'styled-components';
import { Metrics } from '../../Themes';
import BaseText from '../Hoc/BaseText';

const HeaderText = Styled(BaseText)`
    font-size : ${Metrics.Font.Header}px;
`;

export default HeaderText;
