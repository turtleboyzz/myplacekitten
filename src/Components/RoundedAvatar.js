import React from 'react';
import FastImage from 'react-native-fast-image';
import { Colors, Metrics } from '../Themes';
import { FixedWrapper } from '.';

export default function RoundedAvatar({ source = '', size = Metrics.Image.Avatar, backgroundColor = Colors.Primary }) {
  return (
    <FixedWrapper
      width={size + Metrics.Divider * 2.5}
      paddingVertical={Metrics.Divider * 2}
      backgroundColor={backgroundColor}
      borderRadius={size}>
      <FastImage
        style={{
          height: size,
          width: size,
          backgroundColor: Colors.BackgroundPaper,
          borderRadius: size / 2,
        }}
        source={{
          uri: source,
          priority: FastImage.priority.high,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
    </FixedWrapper>
  );
}
