import React from 'react';
import { TouchableOpacity } from 'react-native';
import _ from 'lodash';
import FastImage from 'react-native-fast-image';
import { Colors, Metrics } from '../Themes';

export default function Avatar({ size = Metrics.Image.Avatar, source = '' }) {
  return (
    <TouchableOpacity
      style={{
        width: size + Metrics.Divider,
        height: size + Metrics.Divider,
        padding: Metrics.Divider,
        borderRadius: Metrics.DefaultRadius,
        backgroundColor: Colors.Divider,
      }}>
      <FastImage
        style={{
          height: size,
          width: size,
          backgroundColor: Colors.BackgroundPaper,
          borderRadius: Metrics.DefaultRadius,
        }}
        source={{ uri: _.isEmpty(source) ? '' : source, priority: FastImage.priority.high }}
        resizeMode={FastImage.resizeMode.cover}
      />
    </TouchableOpacity>
  );
}
