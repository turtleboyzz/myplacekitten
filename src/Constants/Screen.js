export default {
  KITTEN_LIST_SCREEN: 'KittenListScreen',
  KITTEN_DETAIL_SCREEN: 'KittenDetailScreen',
  NO_CONNECTION_SCREEN: 'NoConnectionScreen',
};
