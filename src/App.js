import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import store, { persistor } from './Redux/CreateStore';
import RootNavigation from './Navigation/RootNavigation';

const en = require('./Locales/en.json');
const km = require('./Locales/km.json');

export default function App() {
  React.useEffect(() => {
    i18n.use(initReactI18next).init({
      resources: {
        en: {
          translation: en,
        },
        km: {
          translation: km,
        },
      },
      lng: 'en',
      fallbackLng: 'en',
      interpolation: {
        escapeValue: false,
      },
    });
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RootNavigation />
      </PersistGate>
    </Provider>
  );
}
