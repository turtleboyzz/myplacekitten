import React from 'react';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import { BodyText, FixedWrapper, HeaderText, LayoutScrollView, SizedBox, SplitWrapper } from '../../Components';
import { KittenDetailActions, KittenDetailSelectors } from '../../Redux/KittenDetail';
import { Colors, Metrics } from '../../Themes';

export default function KittenDetailScreen() {
  const dispatch = useDispatch();
  const chosenKitten = useSelector(KittenDetailSelectors.chosenKittenSelector);
  const splitedParagraph = chosenKitten.description.split('\n');

  React.useEffect(() => {
    return () => dispatch(KittenDetailActions.onClearInfo());
  }, []);

  const renderSplittedParagraph = () => {
    return _.map(splitedParagraph, (paragraph, index) => (
      <SplitWrapper key={String(index)} flexDirection="column">
        <BodyText textAlign="justify">{`\r\r${paragraph.trim()}`}</BodyText>
        <SizedBox height={Metrics.DefaultGap} />
      </SplitWrapper>
    ));
  };

  return (
    <LayoutScrollView useScroll>
      <FastImage
        style={{
          height: Metrics.ScreenWidth,
          width: Metrics.ScreenWidth,
          backgroundColor: Colors.BackgroundPaper,
        }}
        source={{
          uri: chosenKitten.image,
          priority: FastImage.priority.high,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <FixedWrapper
        flexDirection="column"
        backgroundColor={Colors.BackgroundPaper}
        paddingHorizontal={Metrics.PageHorizontalPadding}
        paddingVertical={Metrics.PageVerticalPadding}>
        <HeaderText fontWeight="bold">{chosenKitten.name}</HeaderText>
        <SizedBox height={Metrics.LargestGap} />
        {renderSplittedParagraph()}
      </FixedWrapper>
    </LayoutScrollView>
  );
}
