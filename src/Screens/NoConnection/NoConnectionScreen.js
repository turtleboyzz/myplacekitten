import React from 'react';
import { LayoutScrollView } from '../../Components';

export default function NoConnectionScreen() {
  return <LayoutScrollView />;
}
