import React from 'react';
import { Appearance, FlatList, RefreshControl, TextInput } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import { useTranslation } from 'react-i18next';
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux';
import {
  BodyText,
  FixedWrapper,
  IconButton,
  LayoutScrollView,
  LinkButton,
  ParagraphText,
  SizedBox,
  SplitWrapper,
  TitleText,
} from '../../Components';
import { KeyExtractorHelper } from '../../Libs';
import { KittenActions, KittenSelectors } from '../../Redux/Kitten';
import { Colors, Metrics } from '../../Themes';
import KittenListItem from './Components/KittenListItem';

const DEFAULT_INITIAL_VALUE = 10;
console.log('Appearance.getColorScheme()', Appearance.getColorScheme());

export default function KittenListScreen({ navigation }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isRequesting = useSelector(KittenSelectors.isRequestingSelector);
  const kittenList = useSelector(KittenSelectors.kittensSelector);

  const [numberToFetch, setNumberToFetch] = React.useState(DEFAULT_INITIAL_VALUE);
  const [isDialogVisible, setIsDialogVisible] = React.useState(false);

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton
          iconName="options-outline"
          iconColor={Colors.Primary}
          buttonColor="transparent"
          borderColor="transparent"
          onPress={handleOpenDialog}
        />
      ),
    });
    RNBootSplash.hide();
    handleFetchKittenList();
  }, []);

  const handleFetchKittenList = () => {
    dispatch(KittenActions.onFetchKittenListRequest({ count: parseFloat(numberToFetch) }));
  };

  const handleOpenDialog = () => setIsDialogVisible(true);

  const handleCloseDialog = () => setIsDialogVisible(false);

  const handleSubmitCountValue = () => {
    setIsDialogVisible(false);
    handleFetchKittenList();
  };

  const renderModal = () => {
    return (
      <Modal isVisible={isDialogVisible} onDismiss={handleCloseDialog} onBackdropPress={handleCloseDialog}>
        {isDialogVisible && (
          <FixedWrapper
            backgroundColor={Colors.BackgroundPaper}
            flexDirection="column"
            paddingHorizontal={Metrics.PageHorizontalPadding}
            paddingVertical={Metrics.PageVerticalPadding}
            borderRadius={Metrics.DefaultRadius}>
            <TitleText fontWeight="bold">{t('screen.kittenListScreen.dialogTitle')}</TitleText>
            <ParagraphText>{t('screen.kittenListScreen.dialogDescription')}</ParagraphText>
            <TextInput
              style={{
                borderBottomColor: Colors.Divider,
                borderBottomWidth: Metrics.Divider,
                width: '100%',
                color: Colors.TextPrimary,
              }}
              placeholder={t('screen.kittenListScreen.dialogInputPlaceholder')}
              value={String(numberToFetch)}
              onChangeText={val => setNumberToFetch(val)}
              keyboardType="numeric"
              autoFocus
              clearTextOnFocus
              onSubmitEditing={handleSubmitCountValue}
            />
            <SizedBox height={Metrics.LargestGap} />
            <FixedWrapper justifyContent="space-around">
              <LinkButton color={Colors.Error} onPress={handleCloseDialog}>
                {t('button.cancel')}
              </LinkButton>
              <LinkButton color={Colors.Success} onPress={handleSubmitCountValue}>
                {t('button.done')}
              </LinkButton>
            </FixedWrapper>
            <SizedBox height={Metrics.DefaultGap} />
          </FixedWrapper>
        )}
      </Modal>
    );
  };

  const renderKittenItem = ({ item }) => {
    return <KittenListItem chosenKitten={item} />;
  };

  const renderItemSeparator = () => <SizedBox height={Metrics.LargestGap} />;

  const renderEmptyList = () => (
    <SplitWrapper>
      <BodyText>{t('screen.kittenListScreen.emptyList')}</BodyText>
    </SplitWrapper>
  );

  return (
    <LayoutScrollView>
      <FlatList
        style={{ width: '100%' }}
        contentContainerStyle={{
          paddingHorizontal: Metrics.PageHorizontalPadding,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        data={kittenList}
        ListHeaderComponent={renderItemSeparator}
        renderItem={renderKittenItem}
        ItemSeparatorComponent={renderItemSeparator}
        ListFooterComponent={renderItemSeparator}
        ListEmptyComponent={renderEmptyList}
        keyExtractor={KeyExtractorHelper}
        refreshControl={
          <RefreshControl
            colors={[Colors.Primary]}
            progressBackgroundColor={Colors.BackgroundPaper}
            refreshing={isRequesting}
            onRefresh={handleFetchKittenList}
          />
        }
      />
      {renderModal()}
    </LayoutScrollView>
  );
}
