import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import styled from 'styled-components/native';
import { CaptionText, FixedWrapper, LinkButton, ParagraphText, SizedBox, SplitWrapper } from '../../../Components';
import RoundedAvatar from '../../../Components/RoundedAvatar';
import { KittenDetailActions } from '../../../Redux/KittenDetail';
import { Colors, Metrics } from '../../../Themes';

const KittenWrapper = styled.View`
  min-height: ${Metrics.DefaultComponentSize}px;
  width: 100%;
  padding: ${Metrics.LargestGap}px;
  border-radius: ${Metrics.DefaultRadius}px;
  background-color: ${Colors.BackgroundPaper};
  flex-direction: row;
`;

export default function KittenListItem({ chosenKitten = {} }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { image, name, description } = chosenKitten;

  const handleViewKittenDetail = () => {
    dispatch(KittenDetailActions.onFetchKittenDetailRequest({ chosenKitten }));
  };
  return (
    <KittenWrapper>
      <RoundedAvatar source={image} backgroundColor={Colors.PrimaryDark} />
      <SizedBox width={Metrics.LargestGap} />
      <SplitWrapper flexDirection="column" justifyContent="center" alignItems="flex-start">
        <FixedWrapper flexDirection="column" justifyContent="space-between">
          <CaptionText>{name}</CaptionText>
          <ParagraphText style={{ flex: 1 }} color={Colors.TextSecondary} numberOfLines={3} alignSelf="flex-start">
            {description}
          </ParagraphText>
        </FixedWrapper>
        <LinkButton onPress={handleViewKittenDetail} style={{ alignSelf: 'flex-start', paddingHorizontal: 0 }}>
          {t('screen.kittenListScreen.learnMoreButton')}
        </LinkButton>
      </SplitWrapper>
    </KittenWrapper>
  );
}
