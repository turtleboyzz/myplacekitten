import { put, call, takeLatest } from 'redux-saga/effects';
import faker from 'faker';
import _ from 'lodash';
import i18next from 'i18next';
import Config from 'react-native-config';
import { REQUEST } from '../../Libs/CreateRequestTypes';
import {
  // Kitten: IMPORT CONSTANT HERE
  FETCH_KITTEN_LIST,
  KITTEN,
  KittenActions,
} from './Action';
import { AlertError, ErrorMessage, LoadingModal, NetworkHelper } from '../../Utils';

// ====================================== Functions ======================================

function* generateSingleKitten() {
  const name = yield call(faker.name.firstName);
  const description = yield call(faker.lorem.paragraphs);
  const imageSeed = yield call(faker.datatype.number, { min: 200, max: 500 });

  return { name, description, image: `${Config.IMAGE_URL}/${imageSeed}/${imageSeed}` };
}

function* recursivelyGenerateKitten(count = 0, allKittens = {}) {
  if (count <= 0) return allKittens;
  const newKitten = yield call(generateSingleKitten);
  const isDupped = !_.isEmpty(allKittens[newKitten.image]);
  if (isDupped) return yield call(recursivelyGenerateKitten, count, allKittens);

  allKittens[newKitten.image] = newKitten;

  if (count - 1 <= 0) return allKittens;
  return yield call(recursivelyGenerateKitten, count - 1, allKittens);
}

// Kitten GENERATORS GO HERE

function* handleFetchKittenListRequest({ payload }) {
  try {
    yield call(LoadingModal.show);

    const isConnected = yield call(NetworkHelper.isNetworkAvailable);
    if (!isConnected) throw i18next.t('message.noConnectionError');

    const { count } = payload;
    const generatedKittens = yield call(recursivelyGenerateKitten, count);
    yield put(KittenActions.onFetchKittenListSuccess({ kittens: Object.values(generatedKittens) }));
    yield call(LoadingModal.hide);
  } catch (error) {
    console.log(`HandleFetchKittenListRequestError:`, error);
    yield call(LoadingModal.hide);
    yield put(KittenActions.onFetchKittenListFailure());
    yield call(AlertError, ErrorMessage(error));
  }
}

function* handleKittenRequest({ payload }) {
  try {
    yield call(LoadingModal.show);
    yield put(KittenActions.onKittenSuccess(payload));
    yield call(LoadingModal.hide);
  } catch (error) {
    console.log('HandleKittenRequestError: ', error);
    yield call(LoadingModal.hide);
    yield put(KittenActions.onKittenFailure());
    yield call(AlertError, ErrorMessage(error));
  }
}

// ====================================== WATCHERS ======================================

// Kitten WATCHERS GO HERE

export function* watchFetchKittenListRequest() {
  yield takeLatest(FETCH_KITTEN_LIST[REQUEST], handleFetchKittenListRequest);
}

export function* watchKittenRequest() {
  yield takeLatest(KITTEN[REQUEST], handleKittenRequest);
}
