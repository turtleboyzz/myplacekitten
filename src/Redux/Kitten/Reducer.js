import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';
import { createReducer, REQUEST, SUCCESS, FAILURE } from '../../Libs';
import {
  // Kitten ACTIONS GO HERE
  FETCH_KITTEN_LIST,
} from './Action';

// ================ SELECTORS ================

// Kitten SELECTORS GO HERE
const kittenReducerSelected = state => state.KittenReducer;
const isRequestingSelector = createSelector(kittenReducerSelected, KittenReducer => KittenReducer.isRequesting);
const kittensSelector = createSelector(kittenReducerSelected, KittenReducer => KittenReducer.kittens);
const chosenKittenSelector = createSelector(kittenReducerSelected, KittenReducer => KittenReducer.chosenKitten);

export const KittenSelectors = {
  // Kitten EXPORT SELECTORS GO HERE
  isRequestingSelector,
  kittensSelector,
  chosenKittenSelector,
};

// ================ REDUCERS ================
const initialState = Immutable({
  isRequesting: false,
  kittens: [],
  chosenKitten: {},
});

export const KittenReducer = createReducer(initialState, {
  // Kitten REDUCERS GO HERE
  [FETCH_KITTEN_LIST[REQUEST]]: state => {
    return state.merge({
      isRequesting: true,
    });
  },
  [FETCH_KITTEN_LIST[SUCCESS]]: (state, action) => {
    const { kittens } = action.response;
    return state.merge({
      isRequesting: false,
      kittens,
    });
  },
  [FETCH_KITTEN_LIST[FAILURE]]: state => {
    return state.merge({
      isRequesting: false,
    });
  },
});
