import { createAction, createRequestTypes, REQUEST, SUCCESS, FAILURE } from '../../Libs';

// ================ CONSTANTS ================

// Kitten CONSTANTS GO HERE
export const FETCH_KITTEN_LIST = createRequestTypes('FETCH_KITTEN_LIST');
export const KITTEN = createRequestTypes('KITTEN');

// ================ ACTIONS ================

export const KittenActions = {
  // Kitten ACTIONS GO HERE
  onFetchKittenListRequest: payload => createAction(FETCH_KITTEN_LIST[REQUEST], { payload }),
  onFetchKittenListSuccess: response => createAction(FETCH_KITTEN_LIST[SUCCESS], { response }),
  onFetchKittenListFailure: () => createAction(FETCH_KITTEN_LIST[FAILURE]),
  onKittenRequest: payload => createAction(KITTEN[REQUEST], { payload }),
  onKittenSuccess: response => createAction(KITTEN[SUCCESS], { response }),
  onKittenFailure: () => createAction(KITTEN[FAILURE]),
};
