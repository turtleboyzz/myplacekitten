import { persistReducer, persistStore } from 'redux-persist';
import { compose, applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import AsyncStorage from '@react-native-community/async-storage';
import Config from 'react-native-config';
import rootSaga from './RootSaga';
import { ImmutablePersistenceTransform } from '../Transforms';
import { reducersCombined } from './CombineReducers';

// const migrations = {
//   1: (state) => {
//     return {
//       ...state,
//     };
//   },
// };

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

if (Config.ENV !== 'PRO') {
  const loggerMiddleware = createLogger();
  middlewares.push(loggerMiddleware);
}

const reducers = (state, action) => {
  return reducersCombined(state, action);
};

const persistConfig = {
  key: 'primary',
  storage: AsyncStorage,
  whitelist: ['KittenReducer'],
  version: 1,
  transforms: [ImmutablePersistenceTransform],
  //   migrate: createMigrate(migrations),
};

const enhancer = compose(applyMiddleware(...middlewares));

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(persistedReducer, enhancer);
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;
export { persistor };
