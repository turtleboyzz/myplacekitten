import { combineReducers } from 'redux';
// PLOP: IMPORT REDUCER HERE
import { KittenDetailReducer } from './KittenDetail';
import { KittenReducer } from './Kitten';

/* ------------- Assemble The Reducers ------------- */
export const reducersCombined = combineReducers({
  // PLOP: COMBINE REDUCER HERE
  KittenDetailReducer,
  KittenReducer,
});
