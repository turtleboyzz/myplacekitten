import { all, fork } from 'redux-saga/effects';

// PLOP: IMPORT WATCHER HERE
import {
  // KittenDetail: IMPORT WATCHER HERE
  watchFetchKittenDetailRequest,
  watchKittenDetailRequest,
} from './KittenDetail';
import {
  // Kitten: IMPORT WATCHER HERE
  watchFetchKittenListRequest,
  watchKittenRequest,
} from './Kitten';

export default function* root() {
  yield all([
    // PLOP: FORK WATCHER HERE

    // KittenDetail: FORK WATCHER HERE
    fork(watchFetchKittenDetailRequest),
    fork(watchKittenDetailRequest),

    // Kitten: FORK WATCHER HERE
    fork(watchFetchKittenListRequest),
    fork(watchKittenRequest),
  ]);
}
