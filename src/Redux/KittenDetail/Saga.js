import { put, call, takeLatest } from 'redux-saga/effects';
import { REQUEST } from '../../Libs/CreateRequestTypes';
import {
  // KittenDetail: IMPORT CONSTANT HERE
  FETCH_KITTEN_DETAIL,
  KITTEN_DETAIL,
  KittenDetailActions,
} from './Action';
import { AlertError, ErrorMessage, LoadingModal } from '../../Utils';
import NavigationHelper from '../../Navigation/NavigationHelper';
import { ScreenConstant } from '../../Constants';

// ====================================== Functions ======================================

// KittenDetail GENERATORS GO HERE

function* handleFetchKittenDetailRequest({ payload }) {
  try {
    yield call(LoadingModal.show);
    const { chosenKitten } = payload;
    yield put(KittenDetailActions.onFetchKittenDetailSuccess({ chosenKitten }));
    yield call(LoadingModal.hide);

    yield call(NavigationHelper.navigate, ScreenConstant.KITTEN_DETAIL_SCREEN);
  } catch (error) {
    console.log(`HandleFetchKittenDetailRequestError:`, error);
    yield call(LoadingModal.hide);
    yield put(KittenDetailActions.onFetchKittenDetailFailure());
    yield call(AlertError, ErrorMessage(error));
  }
}

function* handleKittenDetailRequest({ payload }) {
  try {
    yield call(LoadingModal.show);
    yield put(KittenDetailActions.onKittenDetailSuccess(payload));
    yield call(LoadingModal.hide);
  } catch (error) {
    console.log('HandleKittenDetailRequestError: ', error);
    yield call(LoadingModal.hide);
    yield put(KittenDetailActions.onKittenDetailFailure());
    yield call(AlertError, ErrorMessage(error));
  }
}

// ====================================== WATCHERS ======================================

// KittenDetail WATCHERS GO HERE

export function* watchFetchKittenDetailRequest() {
  yield takeLatest(FETCH_KITTEN_DETAIL[REQUEST], handleFetchKittenDetailRequest);
}

export function* watchKittenDetailRequest() {
  yield takeLatest(KITTEN_DETAIL[REQUEST], handleKittenDetailRequest);
}
