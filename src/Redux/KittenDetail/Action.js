import { createAction, createRequestTypes, REQUEST, SUCCESS, FAILURE } from '../../Libs';

// ================ CONSTANTS ================

// KittenDetail CONSTANTS GO HERE
export const CLEAR_INFO = 'CLEAR_INFO';
export const FETCH_KITTEN_DETAIL = createRequestTypes('FETCH_KITTEN_DETAIL');
export const KITTEN_DETAIL = createRequestTypes('KITTEN_DETAIL');

// ================ ACTIONS ================

export const KittenDetailActions = {
  // KittenDetail ACTIONS GO HERE
  onClearInfo: response => createAction(CLEAR_INFO, { response }),
  onFetchKittenDetailRequest: payload => createAction(FETCH_KITTEN_DETAIL[REQUEST], { payload }),
  onFetchKittenDetailSuccess: response => createAction(FETCH_KITTEN_DETAIL[SUCCESS], { response }),
  onFetchKittenDetailFailure: () => createAction(FETCH_KITTEN_DETAIL[FAILURE]),
  onKittenDetailRequest: payload => createAction(KITTEN_DETAIL[REQUEST], { payload }),
  onKittenDetailSuccess: response => createAction(KITTEN_DETAIL[SUCCESS], { response }),
  onKittenDetailFailure: () => createAction(KITTEN_DETAIL[FAILURE]),
};
