import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';
import { createReducer, REQUEST, SUCCESS, FAILURE } from '../../Libs';
import {
  // KittenDetail ACTIONS GO HERE
  CLEAR_INFO,
  FETCH_KITTEN_DETAIL,
} from './Action';

// ================ SELECTORS ================

// KittenDetail SELECTORS GO HERE
const kittenDetailReducerSelected = state => state.KittenDetailReducer;
const isRequestingSelector = createSelector(
  kittenDetailReducerSelected,
  KittenDetailReducer => KittenDetailReducer.isRequesting
);
const chosenKittenSelector = createSelector(
  kittenDetailReducerSelected,
  KittenDetailReducer => KittenDetailReducer.chosenKitten
);

export const KittenDetailSelectors = {
  // KittenDetail EXPORT SELECTORS GO HERE
  isRequestingSelector,
  chosenKittenSelector,
};

// ================ REDUCERS ================
const initialState = Immutable({
  isRequesting: false,
  chosenKitten: {
    name: '',
    description: '',
    image: '',
  },
});

export const KittenDetailReducer = createReducer(initialState, {
  // KittenDetail REDUCERS GO HERE
  [CLEAR_INFO]: () => {
    return initialState;
  },
  [FETCH_KITTEN_DETAIL[REQUEST]]: state => {
    return state.merge({
      isRequesting: true,
    });
  },
  [FETCH_KITTEN_DETAIL[SUCCESS]]: (state, action) => {
    const { chosenKitten } = action.response;
    return state.merge({
      isRequesting: false,
      chosenKitten,
    });
  },
  [FETCH_KITTEN_DETAIL[FAILURE]]: state => {
    return state.merge({
      isRequesting: false,
    });
  },
});
