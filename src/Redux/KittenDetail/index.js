export { KittenDetailActions } from './Action';
export { KittenDetailSelectors, KittenDetailReducer } from './Reducer';
export {
  // KittenDetail WATCHERS GO HERE
  watchFetchKittenDetailRequest,
  watchKittenDetailRequest,
} from './Saga';
