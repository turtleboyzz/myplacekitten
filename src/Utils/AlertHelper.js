import { Alert } from 'react-native';
import i18n from 'i18next';

function AlertError(
  message = i18n.t('message.defaultErrorMessage'),
  callback = () => {},
  title = i18n.t('message.defaultErrorTitle')
) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.ok'),
        onPress: () => {
          callback();
        },
      },
    ],
    { cancelable: false }
  );
}

function Prompt(message, title = i18n.t('message.defaultErrorTitle'), callback = () => {}, onCancel = () => {}) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.no'),
        style: 'cancel',
        onPress: () => onCancel(),
      },
      {
        text: i18n.t('button.yes'),
        onPress: () => callback(),
      },
    ],
    { cancelable: false }
  );
}

function UploadImagePrompt(onLibrary = () => {}, onCamera = () => {}, onCancel = () => {}) {
  Alert.alert(
    i18n.t('message.uploadImagePromptTitle'),
    '',
    [
      {
        text: i18n.t('button.cancel'),
        style: 'cancel',
        onPress: onCancel,
      },
      {
        text: i18n.t('button.library'),
        onPress: () => onLibrary(),
      },
      {
        text: i18n.t('button.camera'),
        onPress: () => onCamera(),
      },
    ],
    { cancelable: true }
  );
}

function Inform(message, callback = () => {}, title = i18n.t('message.defaultInformTitle')) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.ok'),
        onPress: () => callback(),
      },
    ],
    { cancelable: false }
  );
}

export { AlertError, Prompt, Inform, UploadImagePrompt };
