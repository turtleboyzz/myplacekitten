import _ from 'lodash';
import i18n from 'i18next';
// import crashlytics from '@react-native-firebase/crashlytics';

const ErrorMessage = (error, defaultMessage = i18n.t('message.defaultErrorMessage')) => {
  // crashlytics().recordError(error);
  const isString = _.isString(error);
  if (isString) return error;

  // const errorMessage = _.get(error, 'data.message', _.get(error, 'code', _.get(error, 'errors[0]', error.message)));

  const errorMessage = _.get(
    error,
    'data.errors[0]',
    _.get(error, 'code', _.get(error, 'data.message', error.message))
  );

  if (_.isString(errorMessage)) return errorMessage;

  if (_.isArray(errorMessage)) return errorMessage[0];

  return defaultMessage;
  // return errorMessage;
};

export { ErrorMessage };
