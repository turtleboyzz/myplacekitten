import Progress from 'RNProgressHud';

class LoadingModal {
  static show() {
    Progress.show();
  }

  static hide() {
    Progress.dismiss();
  }
}

export default LoadingModal;
