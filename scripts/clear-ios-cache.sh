echo "remove ${HOME}/Library/Caches/CocoaPods"
rm -rf "${HOME}/Library/Caches/CocoaPods"

echo "cd ios/"
cd ios/

echo "remove Pods/"
rm -rf Pods

echo "remove build/"
rm -rf build

echo "remove /Library/Developer/Xcode/DerivedData/"
rm -rf ~/Library/Developer/Xcode/DerivedData/*

echo "run pod install"
pod install

echo "cd back to project root directory/"
cd ..