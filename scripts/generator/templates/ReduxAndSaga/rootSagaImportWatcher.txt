import { 
    // {{pascalCase name}}: IMPORT WATCHER HERE
    watch{{pascalCase name}}Request 
} from './{{pascalCase name}}';