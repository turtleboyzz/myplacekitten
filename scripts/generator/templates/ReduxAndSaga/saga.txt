import { put, call, takeLatest } from 'redux-saga/effects';
import { REQUEST } from '../../Libs/CreateRequestTypes';
import { 
  // {{pascalCase name}}: IMPORT CONSTANT HERE
  {{constantCase name}},
  {{pascalCase name}}Actions, 
  } from './Action';
import { AlertError, ErrorMessage, LoadingModal } from '../../Utils';

// ====================================== Functions ======================================

// {{pascalCase name}} GENERATORS GO HERE

function* handle{{pascalCase name}}Request({payload}) {
  try {
    yield call(LoadingModal.show);
    yield put({{pascalCase name}}Actions.on{{pascalCase name}}Success(payload));
    yield call(LoadingModal.hide);
  } catch (error) {
    console.log('Handle{{pascalCase name}}RequestError: ', error);
    yield call(LoadingModal.hide);
    yield put({{pascalCase name}}Actions.on{{pascalCase name}}Failure());
    yield call(AlertError, ErrorMessage(error));
  }
}

// ====================================== WATCHERS ======================================

// {{pascalCase name}} WATCHERS GO HERE

export function* watch{{pascalCase name}}Request() {
  yield takeLatest({{constantCase name}}[REQUEST], handle{{pascalCase name}}Request)
}
