import { createAction, createRequestTypes, REQUEST, SUCCESS, FAILURE } from '../../Libs';

// ================ CONSTANTS ================

// {{pascalCase name}} CONSTANTS GO HERE
export const {{constantCase name}} = createRequestTypes('{{constantCase name}}');

// ================ ACTIONS ================

export const {{pascalCase name}}Actions = {
  // {{pascalCase name}} ACTIONS GO HERE
  on{{pascalCase name}}Request: payload => createAction({{constantCase name}}[REQUEST], { payload }),
  on{{pascalCase name}}Success: response => createAction({{constantCase name}}[SUCCESS], { response }),
  on{{pascalCase name}}Failure: () => createAction({{constantCase name}}[FAILURE]),
};
