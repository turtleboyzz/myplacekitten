const stringifyRegex = regexFormat => {
  return regexFormat.source;
};

module.exports = function (plop) {
  plop.setGenerator('Create screen', {
    description: 'Application Screen',

    // Inquiery Prompts
    prompts: [
      {
        type: 'input',
        name: 'screenName',
        message: 'Screen name?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../../src/Screens/{{pascalCase screenName}}/{{pascalCase screenName}}Screen.js',
        templateFile: './templates/screen.txt',
      },
      {
        type: 'append',
        path: '../../src/Screens/index.js',
        pattern: /(PLOP: IMPORT SCREEN HERE)/gi,
        template: `export { default as {{pascalCase screenName}}Screen } from './{{pascalCase screenName}}/{{pascalCase screenName}}Screen';`,
      },
    ],
  });

  plop.setGenerator('Create styled component', {
    description: 'Generate styled component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Styled component name?',
      },
      {
        type: 'list',
        name: 'type',
        message: 'What type of component?',
        choices: ['View', 'Text', 'TextInput', 'TouchableOpacity', 'Image'],
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../../src/Components/{{pascalCase name}}.js',
        templateFile: './templates/styledComponent.txt',
      },
      {
        type: 'append',
        path: '../../src/Components/index.js',
        pattern: /(PLOP: EXPORT STYLED COMPONENT HERE)/gi,
        template: `export { default as {{pascalCase name}} } from './{{pascalCase name}}';`,
      },
    ],
  });

  plop.setGenerator('Create hook component', {
    description: 'Generate Hook Component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Hook component name?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../../src/Components/{{pascalCase name}}.js',
        templateFile: './templates/hookComponent.txt',
      },
      {
        type: 'append',
        path: '../../src/Components/index.js',
        pattern: /(PLOP: EXPORT HOOK COMPONENT HERE)/gi,
        template: `export { default as {{pascalCase name}} } from './{{pascalCase name}}';`,
      },
    ],
  });

  plop.setGenerator('Create Redux and Saga', {
    description: 'Generate redux and saga',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the name of Redux and Saga?',
      },
    ],

    actions: data => {
      const forkWatcherPattern = new RegExp(stringifyRegex(/(name: FORK WATCHER HERE)/gi).replace('name', data.name));
      return [
        {
          type: 'add',
          path: '../../src/Redux/{{pascalCase name}}/Action.js',
          templateFile: './templates/ReduxAndSaga/action.txt',
        },
        {
          type: 'add',
          path: '../../src/Redux/{{pascalCase name}}/Reducer.js',
          templateFile: './templates/ReduxAndSaga/redux.txt',
        },
        {
          type: 'add',
          path: '../../src/Redux/{{pascalCase name}}/Saga.js',
          templateFile: './templates/ReduxAndSaga/saga.txt',
        },
        {
          type: 'add',
          path: '../../src/Redux/{{pascalCase name}}/index.js',
          templateFile: './templates/ReduxAndSaga/index.txt',
        },
        {
          type: 'append',
          path: '../../src/Redux/CombineReducers.js',
          pattern: /(PLOP: IMPORT REDUCER HERE)/gi,
          templateFile: './templates/ReduxAndSaga/rootReducerImport.txt',
          unique: true,
        },
        {
          type: 'append',
          path: '../../src/Redux/CombineReducers.js',
          pattern: /(PLOP: COMBINE REDUCER HERE)/gi,
          template: '{{pascalCase name}}Reducer,',
          unique: true,
        },
        {
          type: 'append',
          path: '../../src/Redux/RootSaga.js',
          pattern: /(PLOP: IMPORT WATCHER HERE)/gi,
          templateFile: `./templates/ReduxAndSaga/rootSagaImportWatcher.txt`,
          unique: true,
        },
        {
          type: 'append',
          path: '../../src/Redux/RootSaga.js',
          pattern: /(PLOP: FORK WATCHER HERE)/gi,
          template: '\n\n// {{pascalCase name}}: FORK WATCHER HERE',
        },
        {
          type: 'append',
          path: '../../src/Redux/RootSaga.js',
          pattern: forkWatcherPattern,
          template: 'fork(watch{{pascalCase name}}Request),',
        },
      ];
    },
  });

  plop.setGenerator('Create Redux actions', {
    description: 'Create Constant Action, Request Action or Selectors',
    prompts: [
      {
        type: 'list',
        name: 'actionType',
        message: 'What to create?',
        choices: ['Constant Action', 'Request Action'],
      },
      {
        type: 'input',
        name: 'actionName',
        message: 'What is it called?',
      },
      {
        type: 'input',
        name: 'reduxName',
        message: 'Apply to what redux?',
      },
    ],
    actions: data => {
      const reduxConstantPattern = new RegExp(
        stringifyRegex(/(name CONSTANTS GO HERE)/gi).replace('name', data.reduxName)
      );
      const reduxActionPattern = new RegExp(stringifyRegex(/(name ACTIONS GO HERE)/gi).replace('name', data.reduxName));
      const reduxReducerPattern = new RegExp(
        stringifyRegex(/(name REDUCERS GO HERE)/gi).replace('name', data.reduxName)
      );
      const reduxImportConstant = new RegExp(
        stringifyRegex(/(name ACTIONS GO HERE)/gi).replace('name', data.reduxName)
      );
      const sagaGeneratorPattern = new RegExp(
        stringifyRegex(/(name GENERATORS GO HERE)/gi).replace('name', data.reduxName)
      );
      const sagaWatcherPattern = new RegExp(
        stringifyRegex(/(name WATCHERS GO HERE)/gi).replace('name', data.reduxName)
      );
      const sagaExportWatcherPattern = new RegExp(
        stringifyRegex(/(name WATCHERS GO HERE)/gi).replace('name', data.reduxName)
      );
      const sagaImportConstantPattern = new RegExp(
        stringifyRegex(/(name: IMPORT CONSTANT HERE)/gi).replace('name', data.reduxName)
      );
      const rootSagaImportWatcherPattern = new RegExp(
        stringifyRegex(/(name: IMPORT WATCHER HERE)/gi).replace('name', data.reduxName)
      );
      const rootSagaForkWatcherPattern = new RegExp(
        stringifyRegex(/(name: FORK WATCHER HERE)/gi).replace('name', data.reduxName)
      );

      switch (data.actionType) {
        case 'Constant Action':
          return [
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Action.js',
              pattern: reduxConstantPattern,
              templateFile: `./templates/ConstantAction/reduxConstant.txt`,
            },
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Action.js',
              pattern: reduxActionPattern,
              templateFile: './templates/ConstantAction/reduxAction.txt',
            },
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Reducer.js',
              pattern: reduxReducerPattern,
              templateFile: './templates/ConstantAction/reduxReducer.txt',
            },
            // Import Constant to Reducer
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Reducer.js',
              pattern: reduxImportConstant,
              templateFile: './templates/RequestAction/reduxImportConstant.txt',
            },
          ];
        case 'Request Action':
          return [
            // Create Constant
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Action.js',
              pattern: reduxConstantPattern,
              templateFile: `./templates/RequestAction/reduxConstant.txt`,
            },
            // Create Action
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Action.js',
              pattern: reduxActionPattern,
              templateFile: './templates/RequestAction/reduxAction.txt',
            },
            // Create Reducer
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Reducer.js',
              pattern: reduxReducerPattern,
              templateFile: './templates/RequestAction/reduxReducer.txt',
            },
            // Import Constant to Reducer
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Reducer.js',
              pattern: reduxImportConstant,
              templateFile: './templates/RequestAction/reduxImportConstant.txt',
            },
            // Create Saga Generator
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Saga.js',
              pattern: sagaGeneratorPattern,
              templateFile: './templates/RequestAction/sagaGenerator.txt',
            },
            // Create Saga Watcher
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Saga.js',
              pattern: sagaWatcherPattern,
              templateFile: './templates/RequestAction/sagaWatcher.txt',
            },
            // Export Saga Watcher
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/index.js',
              pattern: sagaExportWatcherPattern,
              templateFile: './templates/RequestAction/sagaExport.txt',
            },
            // Import Constant to Saga
            {
              type: 'append',
              path: '../../src/Redux/{{pascalCase reduxName}}/Saga.js',
              pattern: sagaImportConstantPattern,
              templateFile: './templates/RequestAction/sagaImportConstant.txt',
            },
            // Import Saga Watcher to RootSaga
            {
              type: 'append',
              path: '../../src/Redux/RootSaga.js',
              pattern: rootSagaImportWatcherPattern,
              templateFile: `./templates/RequestAction/rootSagaImportWatcher.txt`,
            },
            // Fork Saga Watcher to RootSaga
            {
              type: 'append',
              path: '../../src/Redux/RootSaga.js',
              pattern: rootSagaForkWatcherPattern,
              templateFile: './templates/RequestAction/rootSagaForkWatcher.txt',
            },
          ];
        default:
          console.log('Running Default Switch');
          return [];
      }
    },
  });
};
