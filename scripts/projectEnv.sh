#!/usr/bin/env bash

# if [ "$ENV" == "uat"  ];
# then
#   echo "Switching to Firebase UAT environment"
#   yes | cp -rf "../google-services.json" ../android/app
#   yes | cp -rf "../GoogleService-Info.plist" ../ios
if [ "$ENV" == "dev"  ];
then
  # echo "Switching to Firebase Dev environment"
  yes | cp -r "firebase/android/google-services-dev.json" "android/app/google-services.json"
  yes | cp -r "firebase/ios/GoogleService-Info-dev.plist" "ios/cttf/GoogleService-Info.plist"
  yes | cp -r "firebase/ios/Info-dev.plist" "ios/cttf/Info.plist"
  yes | cp -r ".env.dev" ".env"
elif [ "$ENV" == "prod"  ];
then
  # echo "Switching to Firebase Production environment"
  yes | cp -r "firebase/android/google-services-prod.json" "android/app/google-services.json"
  yes | cp -r "firebase/ios/GoogleService-Info-prod.plist" "ios/cttf/GoogleService-Info.plist"
  yes | cp -r "firebase/ios/Info-prod.plist" "ios/cttf/Info.plist"
  yes | cp -r ".env.prod" ".env"
fi

echo "Switched To $ENV Successfully"