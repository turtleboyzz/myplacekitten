module.exports = {
  root: true,
  // extends: '@react-native-community',
  extends: ['airbnb', 'prettier', 'prettier/react', 'plugin:prettier/recommended', 'eslint-config-prettier'],
  parser: 'babel-eslint',
  plugins: ['prettier', 'react', 'jsx-a11y', 'import', 'react-hooks', 'redux-saga'],
  env: {
    browser: true,
    jest: true,
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'avoid',
        trailingComma: 'es5',
        semi: true,
        singleQuote: true,
        printWidth: 120,
      },
    ],
    'prefer-destructuring': [
      'error',
      {
        VariableDeclarator: {
          array: false,
          object: true,
        },
        AssignmentExpression: {
          array: true,
          object: false,
        },
      },
      {
        enforceForRenamedProperties: false,
      },
    ],
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 0,
    'import/prefer-default-export': 'off',
    'import/no-cycle': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/no-unescaped-entities': ['error', { forbid: ['>', '}'] }],
    'react/destructuring-assignment': 0,
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: ['.js'],
      },
    ],
    'no-underscore-dangle': ['error', { allow: ['_id'] }],
    'no-param-reassign': 0,
    'no-return-assign': 0,
    'no-console': 0,
    'no-alert': 0,
    'no-undef': 0,
    'no-use-before-define': ['error', { variables: false }],
    'react/prop-types': 0,
    'react/require-default-props': 0,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'off', // Checks effect dependencies
    'no-await-in-loop': 'off',
    'no-useless-escape': 'off',
  },
};
